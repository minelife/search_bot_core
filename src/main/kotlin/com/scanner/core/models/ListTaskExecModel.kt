package com.scanner.core.models

/**
 * This model uses for create list with progress
 * @see ListTaskExecModel
 * @author minelife
 */
data class ListTaskExecModel(
    var botName:String,
    var domain:String,
    var info:String,
    var percentage:Double,
)

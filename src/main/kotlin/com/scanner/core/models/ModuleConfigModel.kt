package com.scanner.core.models

/**
 * This model provide info for config page
 * @see ModuleConfigModel
 * @author minelife
 */
class ModuleConfigModel(
    val name:String,
    val startPage:String,
    val menuPath:String,
    val params:List<ParamProviderModel>?
)
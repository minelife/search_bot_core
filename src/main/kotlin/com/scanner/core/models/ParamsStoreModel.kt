package com.scanner.core.models

/**
 * This model provide info about all config values
 * @see ParamProviderModel
 * @author minelife
 */
class ParamsStoreModel(
    val params:List<ParamProviderModel>
)
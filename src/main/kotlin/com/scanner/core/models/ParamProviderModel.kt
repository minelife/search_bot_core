package com.scanner.core.models

/**
 * This model provide info about config value
 * @see ParamProviderModel
 * @author minelife
 */
class ParamProviderModel (
    val name:String,
    val type:String,
    var value:Any
)
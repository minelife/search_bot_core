package com.scanner.core.interfaces

/**
 * Interface for pages
 *
 * @author minelife
 * @see Page
 */
interface Page {

    /**
     * This function calls when you want to open this page.
     * Or when you open page from jar file
     */
    fun start()

}
package com.scanner.core.database.models

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "nl_wp_sites_storage")
data class SiteStorageItemModel(

    @Column(name = "domain")
    var domain:String = "",

    @Column(name = "script_id")
    var cmsId:Long = 0,

    @Column(name = "script_ver")
    var cmsVer:Int = 0,

    @Column(name = "live")
    var alive:Boolean = true,

    @Column(name = "is_exploitable")
    var isExploitable:Boolean = false,

    @Column(name = "checked_at")
    var checkedAt: Date? = Date()

): BaseDbModel(){

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    var id:Long = 0

    fun markAsNotActive(){
        alive = false
        checkedAt = Date()
        save()
    }

}
package com.scanner.core.database.models

import javax.persistence.*

@Entity
@Table(name = "wp_like_exploited")
data class WpLikeResult (

    @Column(name = "site_id")
    var siteId:Long = 0

): BaseDbModel(){

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    var id:Long = 0
}
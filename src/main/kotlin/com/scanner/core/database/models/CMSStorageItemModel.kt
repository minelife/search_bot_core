package com.scanner.core.database.models

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "cms_storage")
data class CMSStorageItemModel (
    @Column(name = "name")
    var name: String = ""
) : BaseDbModel() {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long = 0

    @Column(name = "created_at")
    var createdAt = Date()

    @Column(name = "updated_at")
    var updatedAt = Date()
}
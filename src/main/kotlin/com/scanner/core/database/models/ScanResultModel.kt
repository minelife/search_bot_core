package com.scanner.core.database.models

import javax.persistence.*

@Entity
@Table(name = "nl_results")
data class ScanResultModel (
    @Column(name = "site_id")
    var siteId:Long = 0,

    @Column(name = "plugin_id")
    var pluginId:Long = 0,

    @Column(name = "themes_id")
    var themeId:Long = 0,

    @Column(name = "version")
    var version:String = ""
): BaseDbModel(){

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    var id:Long = 0
}
package com.scanner.core.database.models

import javax.persistence.*

@Entity
@Table(name = "themes_storage")
data class ThemeStorageItemModel(

    @Column(name = "cms_id")
    var cmsId:String = "",

    @Column(name = "path")
    var path:String = "",

    @Column(name = "name")
    var name:String = "",

    @Column(name = "html_tag")
    var htmlTag:String = "",

    @Column(name = "version_regex")
    var versionRegex:String = "",

    @Column(name = "vulnerable_version")
    var maxVersion:Int = 0,

    @Column(name = "match_priority")
    var matchPriority:Int = 1

) : BaseDbModel() {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    var id:Long = 0

}
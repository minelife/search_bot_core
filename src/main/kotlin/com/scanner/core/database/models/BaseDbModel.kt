package com.scanner.core.database.models

import io.ebean.Model
import javax.persistence.MappedSuperclass

/**
 * This class provide base functionality for db table
 * @author minelife
 * @see BaseDbModel
 */
@MappedSuperclass
abstract class BaseDbModel : Model()
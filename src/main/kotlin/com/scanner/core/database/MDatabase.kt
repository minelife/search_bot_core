package com.scanner.core.database

import io.ebean.DB
import io.ebean.Database
import io.ebean.DatabaseFactory
import io.ebean.config.DatabaseConfig
import java.lang.Exception
import java.util.*

/**
 * The configuration used for creating a Database.
 * <p>
 * Used to programmatically construct an Database and optionally register it
 * with the DB singleton.
 * </p>
 * <pre>{@code
 *
 * Better create singleton of this class
 *
 * val connection = MDatabase().getMainConnection()
 *
 *
 * }</pre>
 *
 * <p>
 * Note that DatabaseConfigProvider provides a standard Java ServiceLoader mechanism that can
 * be used to apply configuration to the DatabaseConfig.
 * </p>
 *
 * @author minelife
 * @see MDatabase
 */
abstract class MDatabase {

    private val cfg = DatabaseConfig()
    private val properties = Properties()
    private val server: Database

    /**
     * Required
     * Provide db name
     */
    protected abstract fun getDatabaseName():String

    /**
     * Required
     * Provide auth data, first user than password
     */
    protected abstract fun getAuth():Pair<String, String>

    /**
     * Required
     * Provide host, only ip or domain. ex. 1.1.1.1 or mydb.com
     */
    protected abstract fun getHost():String

    /**
     * Required
     * Provide port of db
     */
    protected abstract fun getPort():Int

    /**
     * Optional
     * Add additional params after db url. For example if you do not use ssl (useSSL=false).
     * If you want to add 2 params use & symbol. ex param=1&param2=2
     */
    protected open fun additionalParams() = "useSSL=false"

    /**
     * Required
     * Provide db driver
     */
    protected open fun getDriver() = "com.mysql.cj.jdbc.Driver"

    /**
     * Required
     * Provide is server is default
     */
    protected open fun isDefaultServer() = true

    /**
     * Required
     * Provide db Entries.
     *
     * <pre>{@code
     *
     * Example:
     *
     * return listOf(MyDbEntity::class, MyDbEntity2::class)
     *
     * }</pre>
     */
    protected abstract fun getRegisteredEntries():List<Class<*>>

    private fun configure(){
        if (getDatabaseName().isEmpty())
            throw Exception("MDatabase: Database is empty! Please override 'getDatabaseName' method.")
        if (getAuth().first.isEmpty())
            throw Exception("MDatabase: Auth is empty! Please override 'getAuth' method.")
        if (getHost().isEmpty())
            throw Exception("MDatabase: Host is empty! Please override 'getHost' method.")

        properties["ebean.db.ddl.generate"] = "false"
        properties["ebean.db.ddl.run"] = "false"
        properties["datasource.default"] = getDatabaseName()
        properties["datasource.db.username"] = getAuth().first
        properties["datasource.db.password"] = getAuth().second
        properties["datasource.db.databaseUrl"] = "dbc:mysql://${getHost()}:${getPort()}/${getDatabaseName()}?${additionalParams()}"
        properties["datasource.db.databaseDriver"] = getDriver() //"org.h2.Driver"
        cfg.loadFromProperties(properties)
        cfg.isDefaultServer = isDefaultServer()

        getRegisteredEntries().forEach {
            cfg.addClass(it)
        }
    }


    init{
        configure()
        server = DatabaseFactory
            .create(cfg)
    }

    /**
     * Create new connection.
     * It's not necessary, because main connection has pool of requests.
     * @return Database
     */
    fun createAdditionalConnection(): Database {
        val par = DatabaseConfig()
        par.loadFromProperties(properties)
        return DatabaseFactory.create(par)
    }

    /**
     * Returns main connection
     * @return Database
     */
    fun getMainConnection(): Database = DB.getDefault()

}
package com.scanner.core.utils

import kotlin.math.roundToInt

/**
 * This class provides more flexible Logs
 * @author minelife
 */
object Log{
    var logLevel = 3 // 3-ALL , 2 - DEBUG::ERROR , 1 - ERROR, 0 - NOTHING

    private val ANSI_RESET = "\u001B[0m"
    private val ANSI_BLACK = "\u001B[30m"
    private val ANSI_RED = "\u001B[31m"
    private val ANSI_GREEN = "\u001B[32m"
    private val ANSI_YELLOW = "\u001B[33m"
    private val ANSI_BLUE = "\u001B[34m"
    private val ANSI_PURPLE = "\u001B[35m"
    private val ANSI_CYAN = "\u001B[36m"
    private val ANSI_WHITE = "\u001B[37m"

    fun ii(className: String, message: String) { //important info
        if (logLevel == 1 || logLevel == 3) println("$ANSI_PURPLE$className$ANSI_RESET : $ANSI_GREEN$message$ANSI_RESET")
    }

    fun iw(className: String, message: String) { //important warning
        if (logLevel == 3 || logLevel == 2) println("$ANSI_PURPLE$className$ANSI_RESET : $ANSI_YELLOW$message$ANSI_RESET")
    }

    fun ie(className: String, message: String) { //important error
        if (logLevel != 0) println("$ANSI_PURPLE$className$ANSI_RESET : $ANSI_YELLOW$message$ANSI_RESET")
    }

    fun id(className: String, message: String) { //important data
        if (logLevel == 3 || logLevel == 2) println("$ANSI_PURPLE$className$ANSI_RESET : $ANSI_BLUE$message$ANSI_RESET")
    }

    //info
    fun i(className: String, message: String) {
        if (logLevel == 3) println("$ANSI_GREEN$className : $message$ANSI_RESET")
    }

    //error
    fun e(className: String, message: String) {
        if (logLevel != 0) println("$ANSI_RED$className : $message$ANSI_RESET")
    }

    //debug
    fun d(className: String, message: String) {
        if (logLevel == 3 || logLevel == 2) println("$ANSI_BLUE$className : $message$ANSI_RESET")
    }

    //warning
    fun w(className: String, message: String) {
        if (logLevel == 3 || logLevel == 2) println("$ANSI_YELLOW$className : $message$ANSI_RESET")
    }

    fun ai(className: String, message: String) { //accent info
        if (logLevel == 3) println("$ANSI_PURPLE$className$ANSI_RESET : $ANSI_GREEN$message$ANSI_RESET")
    }

    fun aw(className: String, message: String) { //accent warning
        if (logLevel == 3 || logLevel == 2) println("$ANSI_PURPLE$className$ANSI_RESET : $ANSI_YELLOW$message$ANSI_RESET")
    }

    fun ae(className: String, message: String) { //accent error
        if (logLevel != 0) println("$ANSI_PURPLE$className$ANSI_RESET : $ANSI_RED$message$ANSI_RESET")
    }

    fun ad(className: String, message: String) { //accent data
        if (logLevel == 3 || logLevel == 2) println("$ANSI_PURPLE$className$ANSI_RESET : $ANSI_BLUE$message$ANSI_RESET")
    }

    fun progressPercentage(remain: Int, total: Int) {
        progressPercentage(remain, total, null)
    }

    fun clearConsole(){
        System.out.print("\r");
        System.out.flush();

//            print("\u001b[H \u001b[2J")
//            print("\u0033[H \u0033[J")

//            Runtime.getRuntime().exec("clear")
//            System.out.flush()
    }

    fun progressPercentage(remain: Int, total: Int, msg: String?) {
        require(remain <= total)
        val maxBareSize = 100
        val remainProcent = 100f * remain.toFloat() / total.toFloat()
        val defaultChar = '-'
        val icon = "*"
        val bare = String(CharArray(maxBareSize)).replace('\u0000', defaultChar) + "]"
        val bareDone = StringBuilder()
        bareDone.append("[")
        val result = remainProcent.roundToInt()
        for (i in 0 until remainProcent.toInt()) {
            bareDone.append(icon)
        }
        val bareRemain = bare.substring(remainProcent.toInt(), bare.length)
        print("\r" + ANSI_PURPLE + bareDone + bareRemain + ANSI_RESET + " " + ANSI_BLUE + result + "% " + ANSI_RESET + " || " + ANSI_GREEN + remain + " / " + total + ANSI_RESET + if (msg != null) "  ###  $ANSI_CYAN$msg$ANSI_RESET" else "")
        if (remain == total) {
            print("\n")
        }
    }
}
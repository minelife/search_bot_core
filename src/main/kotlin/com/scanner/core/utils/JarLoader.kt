package com.scanner.core.utils

import com.scanner.core.models.ModuleConfigModel
import java.io.File
import java.net.URL
import java.net.URLClassLoader
import java.util.jar.JarFile

/**
 * Object for loads class from jar file
 * @see JarLoader
 * @author minelife
 */
object JarLoader {

    private fun loadClassLoader(file: File):URLClassLoader = URLClassLoader(arrayOf<URL>(file.toURI().toURL()), this.javaClass.classLoader)

    /**
     * This method load all info about jar module
     * @param file File path to jar file
     * @return Class<*>
     */
    fun loadModuleConfig(file: File): ModuleConfigModel?{
        val j = JarFile(file)
        val s = j.getInputStream(j.getJarEntry("moduleConfig.json"))?.readAllBytes()
        if(s == null){
            Log.ae("LoadModuleException", "${file.path} not have moduleConfig.json!!")
            return null
        }
        return getGson().fromJson(String(s), ModuleConfigModel::class.java)
    }

    /**
     * This method load class from jar file
     * @param file File path to jar file
     * @param class String class name
     * @return Class<*>
     */
    fun loadClass(file: File, `class`: String):Class<*>{
        val c = loadClassLoader(file)
        return Class.forName(`class`, true, c)
    }

    /**
     * This method load class from jar file and invoke method with name
     * @param file File path to jar file
     * @param class String class name
     * @param method String method name
     * @return Any
     */
    fun loadClassAndInvokeMethod(file: File, `class`: String, method: String):Any?{
        val loadedClass = loadClass(file, `class`)
        val m = loadedClass.getDeclaredMethod(method)
        val instance = loadedClass.getDeclaredConstructor().newInstance()
        return m.invoke(instance)
    }

    /**
     * This method load class from jar file and invoke method with name
     * @param file File path to jar file
     * @param class String class name
     * @param method String method name
     * @return Any
     */
    fun getCurrentJarConfigModel():ModuleConfigModel{
        val j = String(JarLoader::class.java.classLoader.getResourceAsStream("moduleConfig.json").readAllBytes())
        return getGson().fromJson(j, ModuleConfigModel::class.java)
    }


//    fun loadClassAndInvokeMethod(`class`: String, method: String, vararg params: List<*>):Any{
//        loadClass(`class`)
//        val m = loadedClass.getDeclaredMethod(method)
//
//        val data = ArrayList<Class<*>>()
//        params.forEachIndexed { index, any ->
//            val c = Class.forName(any.javaClass.name)
//            data[index] = c
//        }
//        val instance = loadedClass.getDeclaredConstructor(data.toArray(arrayOfNulls<Class<*>>(data.size))).newInstance(params)
//        return m.invoke(instance)
//    }
}
package com.scanner.core.utils

import com.google.gson.Gson
import okhttp3.Response
import java.util.concurrent.TimeUnit
import kotlin.math.round

/**
 * @author minelife
 */

/**
 * This method calculating time of executing some task
 * @return Long
 */
fun (()->Unit).checkExec():Long{
    val start = System.currentTimeMillis()
    this()
    val ms = System.currentTimeMillis() - start
    Log.ii("CheckTimeExec", "$ms")
    return ms
}

/**
 * This method convert long to string time as 0 days 00:00:00
 * @return String
 */
fun Long.formatTime():String{
    val day = TimeUnit.SECONDS.toDays(this).toInt()
    val hours = TimeUnit.SECONDS.toHours(this) - TimeUnit.DAYS.toHours(day.toLong())
    val minute = TimeUnit.SECONDS.toMinutes(this) -
            TimeUnit.DAYS.toMinutes(day.toLong()) -
            TimeUnit.HOURS.toMinutes(hours)
    val second = TimeUnit.SECONDS.toSeconds(this) -
            TimeUnit.DAYS.toSeconds(day.toLong()) -
            TimeUnit.HOURS.toSeconds(hours) -
            TimeUnit.MINUTES.toSeconds(minute)
    return ((if(day>0)"$day days " else "") +
            (if(hours>0)"${if (hours<10)"0$hours" else "$hours"}:" else "") +
            "${if(minute<10)"0$minute" else "$minute"}:" +
            if(second<10)"0$second" else "$second")
}

/**
 * This method builds string to correct url
 * @return String
 */
fun String.formatURL():String{
    val isHttps = this.matches("https://".toRegex())
    val protocol = if (isHttps) "https://" else "http://"
    var a = this.replace("http://","")
        .replace("https://", "")
        .replace("//","")
        .replace(":", "")
        .trim()
        .replace("\r", "")
        .replace("\n", "")
        .split("/")[0]

    a = "$protocol$a/"

    return a
}

/**
 * This method round double to x decimals
 * @param decimals Int
 * @return Double
 */
fun Double.round(decimals: Int): Double {
    var multiplier = 1.0
    repeat(decimals) { multiplier *= 10 }
    return round(this * multiplier) / multiplier
}

/**
 * This method provide url to OkHttpResponse
 * @return String
 */
fun Response.getUrl() = this.request.url.toString()

/**
 * This method returns base Gson for all application
 * @return Gson
 */
private var g:Gson? = null
fun getGson():Gson{
    if(g == null)
        g = Gson()
    return g!!
}
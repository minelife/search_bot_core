package com.scanner.core.utils

import com.scanner.core.models.ParamProviderModel
import com.scanner.core.models.ParamsStoreModel
import java.io.File

/**
 * This class provide global values from config page
 *
 * @author minelife
 * @see GlobalParamsProvider
 */
class GlobalParamsProvider{

    companion object{
        private fun getUserDataDirectory(): String = System.getProperty("user.home") + "${File.separator}.scannerStore${File.separator}"
        private fun getUserDataFile(): String = "configs.json"

        private var data = ParamsStoreModel(ArrayList())

        /**
         * Method for read setting file again
         */
        fun forceUpdateData(){
            createFileIfNotExist()
            val json = File(getUserDataDirectory()+getUserDataFile()).readText()
            data = getGson().fromJson(json, ParamsStoreModel::class.java)
        }

        /**
         * Method for saving data
         * @param name String
         */
        fun getString(name: String) = data.params.first { it.name == name }.value as String

        /**
         * Method for get number from setting page
         * @param name String
         */
        fun getNumber(name: String) = data.params.first { it.name == name }.value as Number

        /**
         * Method for get boolean from settings page
         * @param name String
         */
        fun getBoolean(name: String) = data.params.first { it.name == name }.value as Boolean

        private fun createFileIfNotExist(){
            val f = File(getUserDataDirectory()+getUserDataFile())
            if(!f.exists())
                saveToFile()
        }

        private fun saveToFile(text:String = getGson().toJson(data)){
            File(getUserDataDirectory()).mkdirs()
            File(getUserDataDirectory()+getUserDataFile()).writeText(text)
        }
    }

    init {
        createFileIfNotExist()
        forceUpdateData()
    }

    fun getData() = data

    /**
     * System method for saving data
     * @param d ParamsStoreModel
     */
    fun saveData(d: ParamsStoreModel){
        data = d
        saveToFile()
    }

    /**
     * System method for saving data when added new plugin
     * @param d ParamsStoreModel
     */
    fun forceUpdateParams(params:List<ParamProviderModel>){
        createFileIfNotExist()
        val json = File(getUserDataDirectory() + getUserDataFile()).readText()
        val oldParams = getGson().fromJson(json, ParamsStoreModel::class.java).params.toMutableList()

        params.forEach { p->
            if(oldParams.none { it.name == p.name }){
                oldParams.add(p)
            }
        }

        data = ParamsStoreModel(oldParams)
        saveToFile()
    }


}
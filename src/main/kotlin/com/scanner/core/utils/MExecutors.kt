package com.scanner.core.utils

import java.util.concurrent.ExecutorService
import java.util.concurrent.ThreadPoolExecutor

/**
 * Class with thread pool executors
 * @see MExecutors
 */
abstract class MExecutors(threads:Int){

    private val executorParser: ThreadPoolExecutor =  java.util.concurrent.Executors.newFixedThreadPool(threads) as ThreadPoolExecutor

    /**
     * Get current Executor
     * @return ExecutorService
     */
    fun getExecutorParser():ExecutorService{
        return executorParser
    }

    /**
     * Returns is executor terminated
     * @return Boolean
     */
    fun isExecutorParserTerminated():Boolean{
        return executorParser.isTerminated
    }

    /**
     * Returns count of all tasks
     * @return Long
     */
    fun getTaskCount():Long{
        return executorParser.taskCount
    }

    /**
     * Returns count of executed tasks
     * @return Long
     */
    fun getExecuted():Long{
        return executorParser.completedTaskCount
    }

    /**
     * Returns how many tasks left
     * @return Long
     */
    fun getLeftTasks():Long{
        return executorParser.taskCount - executorParser.completedTaskCount
    }

}
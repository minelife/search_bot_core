package com.scanner.core.ui.controllers

import com.scanner.core.models.ParamProviderModel
import com.scanner.core.models.ParamsStoreModel
import com.scanner.core.ui.UIController
import com.scanner.core.ui.view.NumberTextField
import com.scanner.core.utils.GlobalParamsProvider
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList

abstract class UISettingsPageController : UIController(){

    private val globalParamsProvider = GlobalParamsProvider()
    private val params = ArrayList<ParamProviderModel>()

    abstract fun getRootNodeForParams(): VBox
    open fun getTextColor() = "#5cc87c"

    fun saveSettings(){
        getRootNodeForParams().children.forEach { p->
            val txt = (p as HBox).children[1] as TextField
            val arg = globalParamsProvider.getData().params.first{it.name == txt.id}
            arg.value = if(arg.type.toLowerCase() == "number") (txt as NumberTextField).getNumber() else txt.text
            params.add(arg)
        }
        globalParamsProvider.saveData(ParamsStoreModel(params))
    }

    fun buildSettingsPanel(){
        globalParamsProvider.getData().params.forEach {
            getRootNodeForParams().children.add(createField(it.name, it.value, it.type.toLowerCase() == "number"))
        }
    }

    private fun createField(name: String, value: Any, isNumber: Boolean):Node{
        val l = Label("$name: ")
        l.alignment = Pos.BASELINE_CENTER
        HBox.setMargin(l, Insets(0.0, 8.0, 0.0, 8.0))
        l.textFill = Color.web(getTextColor())
        val h = getHBox(l)
        val e = if(isNumber)NumberTextField() else TextField()
        e.id = name
        e.text = if(isNumber)(value as Number).toString() else value.toString()
        h.children.add(e)
        return h
    }

    private fun getHBox(node: Node): HBox {
        val h = HBox()
        h.alignment = Pos.BASELINE_CENTER
        h.isFillHeight = true
        h.children.add(node)
        VBox.setMargin(h, Insets(4.0, 0.0, 4.0, 0.0))
        h.alignment = Pos.TOP_CENTER
        return h
    }

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        GlobalParamsProvider.forceUpdateData()
        buildSettingsPanel()
    }
}
package com.scanner.core.ui.pages

import com.scanner.core.interfaces.Page
import com.scanner.core.utils.JarLoader
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage
import java.io.File
import java.lang.Exception

/**
 * Class for creating Page
 * @author minelife
 */
abstract class UIPage : Page {

    /**
     * Override this method to change window size
     * Pair(Double, Double): width, height
     * @return Pair
     */
    protected open fun getPageSize() = Pair(400.0, 600.0)

    /**
     * Override this method to change window title
     * @return String
     */
    protected open fun getPageTitle() = ""

    /**
     * Override this method to change window style
     * @return String
     */
    protected open fun getPageStyleFile() = "/styles/main.css"

    /**
     * This method load fxml file and set apply params to stage
     * @param String path to fxml file
     * @param Stage
     */
    protected fun loadXml(fxml: String, stage: Stage) {
        val root = FXMLLoader.load<Parent>(javaClass.getResource(fxml))
        val scene = Scene(root)
        stage.scene = scene
        try {
            scene.stylesheets.add(javaClass.getResource(getPageStyleFile()).toExternalForm())
        }catch (e:Exception){e.printStackTrace()}
        stage.title = getPageTitle()
        stage.width = getPageSize().first
        stage.height = getPageSize().second
        stage.show()
    }

    /**
     * This method load fxml file and set apply params to stage
     * @param String path to fxml file
     * @param Stage
     */
    protected fun loadWithNode(node: Parent, stage: Stage) {
        val scene = Scene(node)
        stage.scene = scene
        try {
            scene.stylesheets.add(javaClass.getResource(getPageStyleFile()).toExternalForm())
        }catch (e:Exception){e.printStackTrace()}
        stage.title = getPageTitle()
        stage.width = getPageSize().first
        stage.height = getPageSize().second
        stage.show()
    }

    /**
     * This method open page from jar module
     * @param File path to jar file
     * @param String main page class name with package name
     */
    protected fun openPageFromJar(jarFile: File, className:String) {
        JarLoader.loadClassAndInvokeMethod(jarFile, className, "start")
    }
}
package com.scanner.core.ui.pages

/**
 * This class is the base class for the exploit page. You have to extends from it if you want to create your custom exploit page.
 * @param T is a type of value u want to receive from main menu.
 * @author minelife
 * @see UIPageWithArgs
 */
abstract class UIPageWithArgs<T>: UIPage() {

    /**
     * This method is analog of start method for default page, but receive arguments
     */
    abstract fun start(arg:T)

    @Deprecated("Don't override and call this method!!!")
    override fun start() {}
}
package com.scanner.core.ui.pages

/**
 * This class is the base class for the exploit page with site list R type.
 * @param R type of . ex String or SiteStorageItemModel
 * @author minelife
 * @see UIPageWithArgs
 */
abstract class UIPageWithList<R> : UIPageWithArgs<List<R>>() {

    @Deprecated("Don't override and call this method!!!")
    override fun start() {}
}
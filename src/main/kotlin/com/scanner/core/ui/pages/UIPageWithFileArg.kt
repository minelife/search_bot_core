package com.scanner.core.ui.pages

import java.io.File

/**
 * This class is the base class for the exploit page with file.
 * @author minelife
 * @see UIPageWithArgs
 */
abstract class UIPageWithFileArg : UIPageWithArgs<File>() {

    @Deprecated("Don't override and call this method!!!")
    override fun start() {}
}
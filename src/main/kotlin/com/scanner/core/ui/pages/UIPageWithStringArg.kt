package com.scanner.core.ui.pages

/**
 * This class is the base class for the exploit page for one site.
 * @author minelife
 * @see UIPageWithArgs
 */
abstract class UIPageWithStringArg: UIPageWithArgs<String>() {

    @Deprecated("Don't override and call this method!!!")
    override fun start() {}
}
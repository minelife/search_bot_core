package com.scanner.core.ui

import javafx.application.Application

/**
 * This class uses for create application
 * @author minelife
 * @see UIApplication
 */
abstract class UIApplication :Application()
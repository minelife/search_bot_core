package com.scanner.core.ui.dialogs

import javafx.application.Platform
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.layout.VBox

class UIDialogBuilder {

    private val dialog: Dialog<Any> = Dialog()
    private val vBox = VBox()

    private var callBack:(ButtonType)->Unit = {}

    fun setTitle(title:String):UIDialogBuilder{
        dialog.title = title
        return this
    }

    fun setHeaderTitle(title: String):UIDialogBuilder{
        dialog.headerText = title
        return this
    }

    fun addButton(type:ButtonType):UIDialogBuilder{
        dialog.dialogPane.buttonTypes.add(type)
        return this
    }

    fun addNode(node:Node):UIDialogBuilder{
        vBox.children.add(node)
        return this
    }

    fun requestFocus(node: Node):UIDialogBuilder{
        Platform.runLater(node::requestFocus)
        return this
    }

    fun addButtonListener(callBack:(ButtonType)->Unit):UIDialogBuilder{
        dialog.setResultConverter { button: ButtonType -> callBack(button)}
        return this
    }

    fun build():Dialog<Any>{
        dialog.dialogPane.content = vBox
        return dialog
    }

    fun buildAndShow(waitResult:Boolean){
        val f = build()
        if(waitResult)
            f.showAndWait()
        else
            f.show()
    }
}
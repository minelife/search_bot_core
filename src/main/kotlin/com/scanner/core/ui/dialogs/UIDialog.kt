package com.scanner.core.ui.dialogs

import javafx.scene.control.*
import javafx.scene.control.Alert.AlertType
import javafx.stage.FileChooser
import javafx.stage.Stage
import java.io.File


object UIDialog {

    /**
     *             AlertType.CONFIRMATION
     */
    fun alert(type: AlertType, content: String, onButtonPressed: (ButtonType) -> Unit, vararg buttons: ButtonType){
        val alert = Alert(type, content, *buttons)
        alert.showAndWait()
        onButtonPressed(alert.result)
    }

    fun buildCustom(): UIDialogBuilder = UIDialogBuilder()

    fun showFileChooser(onSelected:(File)->Unit, onCanceled:()->Unit = {}, title: String = "Select File", vararg filter: FileChooser.ExtensionFilter){
        val fileChooser = FileChooser()
        fileChooser.title = title
        if(filter.isNotEmpty())
            fileChooser.extensionFilters.addAll(filter)
        val f = fileChooser.showOpenDialog(Stage())
        if(f!=null)
            onSelected(f)
        else
            onCanceled()
    }
}
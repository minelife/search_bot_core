package com.scanner.core.ui

import com.scanner.core.utils.JarLoader
import javafx.fxml.Initializable
import javafx.scene.Node
import java.io.File

/**
 * This class uses to create the controller for view
 * @author minelife
 * @see UIController
 */
abstract class UIController: Initializable{

    /**
     * This method hide parent page
     * @param Node
     */
    protected fun hideThisPage(node: Node){
        node.scene.window.hide()
    }

    /**
     * Find by css id, not FX:ID !!!!
     * @param Node any node in this scene
     * @param Strind id of view
     */
    protected fun findByID(any: Node, id:String):Node = any.scene.lookup("#$id")

    /**
     * This method opens start page from jar module
     * @param File path to jar file
     * @param className main page class
     */
    protected fun openPageFromJar(jarFile: File, className:String) {
        JarLoader.loadClassAndInvokeMethod(jarFile, className, "start")
    }
}
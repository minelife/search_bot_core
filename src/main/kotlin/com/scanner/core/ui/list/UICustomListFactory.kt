package com.scanner.core.ui.list

import javafx.fxml.FXMLLoader
import javafx.scene.control.*
import java.io.IOException

/**
 *TThis class uses for create custom item of list
 * @see UICustomListFactory
 * @author minelife
 */
abstract class UICustomListFactory <T> : ListCell<T>() {

    /**
     * Override this method to change path to position
     * @return String
     */
    protected abstract fun getViewPath():String

    /**
     * This method calls when need to change status of view. Ex update text, progress etc.
     */
    protected abstract fun onUpdate(item: T)

    private fun loadFXML() {
        try {
            val loader = FXMLLoader(javaClass.getResource(getViewPath()))
            loader.setController(this)
            loader.setRoot(this)
            loader.load<Any>()
        } catch (e: IOException) {
            throw RuntimeException(e)
        }
    }

    @Deprecated("Do not override this method!!!")
    override fun updateItem(item: T?, empty: Boolean) {
        super.updateItem(item, empty)
        if (empty) {
            text = null
            contentDisplay = ContentDisplay.TEXT_ONLY
        } else {
            onUpdate(item!!)
            contentDisplay = ContentDisplay.GRAPHIC_ONLY
        }
    }

    init {
        loadFXML()
    }
}
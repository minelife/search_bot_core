package com.scanner.core.ui.list

import com.scanner.core.models.ListTaskExecModel
import javafx.fxml.FXML
import javafx.scene.control.ProgressBar
import javafx.scene.control.TextField

/**
 *TThis class uses for create item of list with progress
 * @see TasksListFactory
 * @author minelife
 */

class TasksListFactory : UICustomListFactory<ListTaskExecModel>(){

    @FXML
    private lateinit var titleLabel: TextField

    @FXML
    private lateinit var domainLabel: TextField

    @FXML
    private lateinit var infoLabel: TextField

    @FXML
    private lateinit var progressBar: ProgressBar

    override fun getViewPath(): String  = "/views/task_cell.fxml"

    override fun onUpdate(item: ListTaskExecModel) {
        titleLabel.text = item.botName
        domainLabel.text = item.domain
        infoLabel.text = item.info
        progressBar.progress = item.percentage/100.0
    }
}
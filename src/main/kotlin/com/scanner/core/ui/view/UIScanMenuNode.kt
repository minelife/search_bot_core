package com.scanner.core.ui.view

import com.scanner.core.interfaces.UIMainExploitMenuViewCallback
import com.scanner.core.ui.dialogs.UIDialog
import com.scanner.core.utils.JarLoader
import javafx.application.Platform
import javafx.scene.control.ButtonType
import javafx.scene.control.TextField

/**
 * This class provide base UI for scan menu
 * @author minelife
 * @see UIScanMenuNode
 */
class UIScanMenuNode: UIBaseNode<UIScanMenuNode>() {

    private var title = getLabel(JarLoader.getCurrentJarConfigModel().name)

    private fun getUrlDialog(call:(String)->Unit){
        val edit = TextField()
        UIDialog
            .buildCustom()
            .setTitle("One Site")
            .setHeaderTitle("Enter site url:")
            .addNode(edit)
            .addButton(ButtonType.CANCEL)
            .addButton(ButtonType.APPLY)
            .addButtonListener {
                if(it == ButtonType.APPLY)
                    call(edit.text)
            }
            .buildAndShow(true)
    }

    /**
     * Method for change title of page
     * @param title String
     * @return UIScanMenuNode
     */
    fun setTitle(title:String):UIScanMenuNode{
        Platform.runLater {
            this.title.text = title
        }
        return this
    }

    /**
     * Method to add base scan menu
     * @param c UIMainExploitMenuViewCallback, callback
     * @return UIScanMenuNode
     */
    fun addBaseScanMenu(c: UIMainExploitMenuViewCallback):UIScanMenuNode{
        Platform.runLater {
            addButton("One Site") {
                getUrlDialog {
                    c.onScanOneSite(it)
                }
            }
            addButton("Sites from file") {
                UIDialog.showFileChooser({
                    c.onScanFileListSite(it)
                })
            }
            addButton("Sites from db") {
                c.onScanSitesFromDB()
            }
        }

        return this
    }

    init {
        root.children.add(getHBox(title))
        root.children.add(getSeparator())
    }
}
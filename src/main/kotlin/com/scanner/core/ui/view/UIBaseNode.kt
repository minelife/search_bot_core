package com.scanner.core.ui.view

import javafx.application.Platform
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.TextArea
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import kotlin.system.exitProcess

/**
 * Class that provides base functionality to makes pages dynamically without fxml file and controller.
 * @property C is a children class
 * @author minelife
 * @see UIBaseNode
 */
abstract class UIBaseNode<C:UIBaseNode<C>> {

    private fun getThis() = this as C

    /**
     * Root node for page
     * @return Parent
     */
    protected open var root = getVBox()

    /**
     * Returns Button node
     * @param text String, text in button
     * @param click Unit, callback on button pressed
     * @return Button
     */
    protected fun getButton(text: String, click: () -> Unit): Button {
        val b = Button(text)
        b.styleClass.add("menu_button")
        b.prefWidth = 200.0
        HBox.setMargin(b, Insets(4.0, 0.0, 4.0, 0.0))
        b.setOnMouseClicked{
            click()
        }
        return b
    }

    /**
     * Returns HBox node
     * @param node Node, optional if u want to add only one children to this
     * @return HBox
     */
    protected fun getHBox(node: Node? = null): HBox {
        val h = HBox()
        h.alignment = Pos.TOP_CENTER
        h.isFillHeight = true
        if(node != null)
            h.children.add(node)
        return h
    }

    /**
     * Returns TextArea node
     * @param text String, optional if u want to add start text
     * @return TextArea
     */
    protected fun getTextArea(text:String = ""): TextArea {
        val l = TextArea(text)
        l.styleClass.add("out_text")
        l.isEditable = false
        l.style = "-fx-background-color:#141728; -fx-text-fill: #5cc87c;"
        l.styleClass.add("scroll_view")
        HBox.setMargin(l, Insets(4.0, 8.0, 4.0, 8.0))
        return l
    }

    /**
     * Returns Label node
     * @param text String text in label
     * @return Label
     */
    protected fun getLabel(text:String): Label {
        val l = Label(text)
        l.styleClass.add("title_text")
        HBox.setMargin(l, Insets(4.0, 0.0, 4.0, 0.0))
        return l
    }

    /**
     * Returns VBox node
     * @return VBox
     */
    protected fun getVBox():VBox{
        val h = VBox()
        h.alignment = Pos.TOP_CENTER
        h.isFillWidth = true
        return h
    }

    /**
     * Returns Space with height and width
     * @param width Double
     * @param height Double
     * @return HBox
     */
    protected fun getSpace(width:Double = 1.0, height:Double = 1.0):Node{
        val h = HBox()
        h.prefHeight = height
        h.maxHeight = height
        h.minHeight = height
        h.prefWidth = width
        h.maxWidth = width
        h.minWidth = width
        return h
    }

    /**
     * Returns line for separate something
     * @return Node
     */
    protected fun getSeparator(): Node {
        val b = getHBox()
        b.style = "-fx-background-color:#5cc87c"
        b.styleClass.add("separator")
        b.prefHeight = 1.0
        b.maxHeight = 1.0
        b.minHeight = 1.0
        VBox.setMargin(b, Insets(4.0, 0.0, 4.0, 0.0))
        return b
    }

    /**
     * This method will add button to page
     * @param text String
     * @param click Unit, calls when button was pressed.
     * @return UIProgressNode
     */
    @Synchronized
    open fun addButton(text: String, click: () -> Unit): C {
        Platform.runLater {
            root.children.add(getHBox(getButton(text, click)))
        }
        return getThis()
    }

    /**
     * This method will add exit button to page
     * @return UIProgressNode
     */
    @Synchronized
    open fun addExitButton(): C {
        Platform.runLater {
            addButton("Exit") { exitProcess(0) }
        }
        return getThis()
    }

    /**
     * This method will add separator to page
     * @return UIProgressNode
     */
    @Synchronized
    open fun addSeparator(): C {
        Platform.runLater {
            root.children.add(getSeparator())
        }
        return getThis()
    }


    /**
     * This method will add TextArea to page
     * @param text String
     * @param onNodeAdded Unit, calls when TextArea added to page. This is an optional value, you will use it if you later want to update this TextArea.
     * @return UIProgressNode
     */
    @Synchronized
    open fun addTextArea(text:String, onNodeAdded: (textArea:TextArea) -> Unit = {}): C {
        val n = getTextArea(text)
        Platform.runLater {
            root.children.add(getHBox(n))
            onNodeAdded(n)
        }
        return getThis()
    }

    /**
     * This method will add label to page
     * @param text String
     * @param onNodeAdded Unit, calls when label added to page. This is an optional value, you will use it if you later want to update this label.
     * @return UIProgressNode
     */
    @Synchronized
    open fun addLabel(text:String, onNodeAdded: (l:Label) -> Unit = {}): C {
        val n = getLabel(text)
        Platform.runLater {
            root.children.add(getHBox(n))
            onNodeAdded(n)
        }
        return getThis()
    }

    /**
     * Return root node to set as root page element
     * @return Parent
     */
    fun getNode(): VBox = root
}
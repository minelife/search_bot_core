package com.scanner.core.ui.view

import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.event.EventHandler
import javafx.scene.control.TextField
import java.lang.Exception
import java.math.BigDecimal
import java.text.NumberFormat

/**
 * Class to create TextField only with numbers
 *
 * @author minelife
 * @see NumberTextField
 * @author minelife
 */
class NumberTextField(value: BigDecimal, private val nf: NumberFormat) : TextField() {
    private val number: ObjectProperty<BigDecimal> = SimpleObjectProperty()

    /**
     * Method to get number from TextField
     * @return BigDecimal
     */
    fun getNumber(): BigDecimal {
        return number.get()
    }

    /**
     * Method to set number to TextField
     * @param value BigDecimal
     */
    fun setNumber(value: BigDecimal) {
        number.set(value)
    }

    /**
     * Method to get number property from TextField
     * @param ObjectProperty<BigDecimal>
     */
    fun numberProperty(): ObjectProperty<BigDecimal> {
        return number
    }

    @JvmOverloads
    constructor(value: BigDecimal = BigDecimal.ZERO) : this(value, NumberFormat.getInstance()) {
        initHandlers()
    }

    private fun initHandlers() {

        onAction = EventHandler {
            parseAndFormatInput()
        }

        focusedProperty().addListener { _, _, newValue ->
            if (!newValue) {
                parseAndFormatInput()
            }
        }

        numberProperty().addListener { _, _, newValue -> text = nf.format(newValue) }
    }

    private fun parseAndFormatInput() {
        try {
            if (text.isNullOrEmpty()) {
                return
            }
            val parsedNumber = nf.parse(text)
            val newValue = BigDecimal(parsedNumber.toString())
            setNumber(newValue)
            selectAll()
        } catch (ex: Exception) {
            text = nf.format(number.get())
        }
    }

    init {
        initHandlers()
        setNumber(value)
    }
}
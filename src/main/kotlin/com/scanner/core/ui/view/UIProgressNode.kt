package com.scanner.core.ui.view

import com.scanner.core.utils.JarLoader
import com.scanner.core.utils.formatTime
import com.scanner.core.utils.round
import javafx.application.Platform
import javafx.scene.control.*
import java.util.*
import kotlin.concurrent.timerTask

/**
 * This class provide base UI for show progress
 * @see UIProgressNode
 * @author minelife
 */
class UIProgressNode : UIBaseNode<UIProgressNode>(){

    private val titleLabel = getLabel("Task 1")
    private val percentageLabel = getLabel("0%")
    private val timeLabel = getLabel("00:00:00")
    private val leftTimeLabel = getLabel("00:00:00")
    private val messageLabel = getLabel("")
    private val tasksMessageLabel = getLabel("0/0")
    private val outTextArea = getTextArea("")
    private val progressBar = ProgressBar()

    private var timer = Timer()
    private val progressPadding = 200
    private var timeCounter = 0L
    private var percentageProgress = 0.0
    private var maxTasks = 0L
    private var currentTask = 0L
    private var previousProgress = 0.0
    private var previousProgressTime = 0L

    /**
     * This method return progress
     * @return Double
     */
    fun getProgress() = percentageProgress

    /**
     * This method return executed tasks
     * @return Long
     */
    fun getExecutedTasks() = currentTask

    /**
     * This method sets title of the page
     * @param title
     * @return UIProgressNode
     */
    @Synchronized
    fun setTitle(title:String): UIProgressNode {
        Platform.runLater {
            this.titleLabel.text = title
        }
        return this
    }

    /**
     * This method updating max tasks value
     * @param max Long
     * @return UIProgressNode
     */
    @Synchronized
    fun setMaxTasks(max:Long): UIProgressNode{
        this.maxTasks = max
        return this
    }

    /**
     * This method updating current tasks value
     * @param current Long
     * @return UIProgressNode
     */
    @Synchronized
    fun updateCurrentTask(current:Long):UIProgressNode{
        this.currentTask = current
        this.percentageProgress = (current.toDouble()/maxTasks.toDouble())*100.0
        updateProgressPercentage(this.percentageProgress)
        return this
    }

    /**
     * This method updating progress percentage
     * @param progress Double
     * @return UIProgressNode
     */
    @Synchronized
    fun updateProgressPercentage(progress:Double): UIProgressNode {
        if(progress>100)
            this.percentageProgress = 100.0
        else
            this.percentageProgress = progress
        Platform.runLater {
            progressBar.progress = this.percentageProgress/100.0
            percentageLabel.text = "${this.percentageProgress.round(2)}%"
        }
        return this
    }

    /**
     * This method updating progress message
     * @param txt String
     * @return UIProgressNode
     */
    @Synchronized
    fun updateMessage(txt:String): UIProgressNode {
        Platform.runLater {
            messageLabel.text = txt
        }
        return this
    }

    /**
     * This method adding message to out textArea
     * @param txt String
     * @return UIProgressNode
     */
    @Synchronized
    fun updateOut(txt:String): UIProgressNode {
        Platform.runLater {
            outTextArea.text += "\n$txt"
            outTextArea.scrollTop = Double.MAX_VALUE
            outTextArea.scrollLeft = .0
        }
        return this
    }

    /**
     * This method automatically updating progress fields with timers
     * @param updateEvery Long, milliseconds, set it if you want to change speed of updating views. Default 1000
     * @return UIProgressNode
     */
    @Synchronized
    fun autoUpdateProgress(updateEvery:Long = 1000):UIProgressNode{
        timer.schedule(timerTask {
            updateTimer(timeCounter.formatTime())

            if(percentageProgress>0){
                updateLeftTime(((timeCounter/percentageProgress)*(100.0-percentageProgress)).toLong().formatTime())
            }else{
                updateLeftTime("59:59:59")
            }

            previousProgressTime = timeCounter
            timeCounter++

        }, 0, updateEvery)
        return this
    }

    /**
     * This method updating label with timer
     * @param txt String, format it as a time. Ex. 00:10:00
     * @return UIProgressNode
     */
    @Synchronized
    fun updateTimer(txt:String): UIProgressNode {
        Platform.runLater {
            timeLabel.text = txt
        }
        return this
    }

    /**
     * This method updating label which show how much time left to finish all tasks
     * @param txt String, format it as a time. Ex. 00:10:00
     * @return UIProgressNode
     */
    @Synchronized
    fun updateLeftTime(txt:String): UIProgressNode {
        Platform.runLater {
            leftTimeLabel.text = txt
        }
        return this
    }

    /**
     * This method stops all timers, clears all messages
     * @return UIProgressNode
     */
    fun clearAll(){
        timer.cancel()
        timer = Timer()
        Platform.runLater {
            percentageProgress = 0.0
            progressBar.progress = 0.0
            percentageLabel.text = "0%"
            timeLabel.text = "00:00:00"
            leftTimeLabel.text = "00:00:00"
            messageLabel.text = ""
            tasksMessageLabel.text = "0/0"
            outTextArea.text = ""
        }
    }

    /**
     * This method building base progress node
     * @return UIProgressNode
     */
    fun addBaseProgressNode():UIProgressNode{
        val v = getVBox()
        val h = getHBox()

        progressBar.prefHeight = 30.0
        progressBar.minHeight = 30.0
        progressBar.maxHeight = 30.0

        Platform.runLater {
            outTextArea.prefHeight = progressBar.scene.height - 0.0
            progressBar.prefWidth = progressBar.scene.width - progressPadding
            progressBar.maxWidth = progressBar.scene.width - progressPadding

            progressBar.scene.widthProperty().addListener { _, _, value ->
                outTextArea.prefHeight = value.toDouble() - 0.0
                progressBar.prefWidth = value.toDouble() - progressPadding
                progressBar.maxWidth = value.toDouble() - progressPadding
            }
        }

        h.children.addAll(progressBar, percentageLabel, getSpace(8.0), timeLabel, Label(" / "), leftTimeLabel)
        v.children.addAll(titleLabel, h)
        if(maxTasks>0)v.children.addAll(getHBox(tasksMessageLabel.apply { text = "0/${maxTasks}" }))
        v.children.addAll(messageLabel, getSeparator() , outTextArea)

        root.children.add(v)
        return this
    }

    init {
        root.children.add(getHBox(getLabel(JarLoader.getCurrentJarConfigModel().name)))
        root.children.add(getSeparator())
    }

}
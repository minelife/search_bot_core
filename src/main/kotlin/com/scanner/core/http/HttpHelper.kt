package com.scanner.core.http

import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import java.io.IOException
import java.time.Duration

/**
 * Class for make http requests
 * @see HttpHelper
 * @author minelife
 */
class HttpHelper {

    private var followRedirects = false
    private var headers = HashMap<String, String>()
    private lateinit var client: OkHttpClient
    private var connectTimeout = Duration.ofSeconds(5)
    private var readTimeout = Duration.ofSeconds(5)
    private var userAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)"
    private var printExceptions = false

    /**
     * Method for change connection timeout
     * Default 5 sec
     * @param connectTimeout Duration
     */
    fun setConnectionTimeout(connectTimeout:Duration): HttpHelper {
        this.connectTimeout = connectTimeout
        return this
    }

    /**
     * Method to print exceptions if it exist
     */
    fun printExceptions(): HttpHelper {
        this.printExceptions = true
        return this
    }

    /**
     * Method for change read timeout
     * Default 5 sec
     * @param readTimeout Duration
     */
    fun setReadTimeout(readTimeout:Duration): HttpHelper {
        this.readTimeout = readTimeout
        return this
    }

    /**
     * Method for change User Agent
     * @param agent String
     */
    fun changeUserAgent(agent: String): HttpHelper {
        userAgent = agent
        return this
    }

    /**
     * Method to set follow redirects
     * Default false
     * @param followRedirects Boolean
     */
    fun setFollowRedirects(followRedirects: Boolean): HttpHelper {
        this.followRedirects = followRedirects
        return this
    }

    /**
     * Method for build OkHttpClient
     */
    private fun buildClient(): OkHttpClient{
        headers["User-Agent"] =  userAgent
        return OkHttpClient
            .Builder()
            .followRedirects(followRedirects)
            .connectTimeout(connectTimeout)
            .readTimeout(readTimeout)
            .build()
    }

    private class MCallback(private val onOk:(HttpResponse)->Unit, private val onError:(e: Exception)->Unit): Callback{
        val resp = HttpResponse("", -1, HashMap(), "")

        override fun onFailure(call: Call, e: IOException) {
            onError(e)
        }

        override fun onResponse(call: Call, response: Response) {
            resp.url = call.request().url.toString()
            resp.code = response.code
            response.headers.forEach {
                resp.header[it.first] = it.second
            }
            resp.body = response.body?.string()?:""
            onOk(resp)
            try {
                response.close()
            }catch (e: Exception){}
        }
    }

    /**
     * Method to make HTTP GET request
     * @param url String site url
     * @param onOk Unit, calls when site returns any response
     * @param onError Unit, calls when was any exception
     */
    fun asyncGet(url: String, onOk:(HttpResponse)->Unit, onError:(e: Exception)->Unit = {}){
        client = buildClient()

        val requestBuilder = Request.Builder().url(url)
        headers.forEach { (t, u) ->
            requestBuilder.addHeader(t, u)
        }

        client.newCall(requestBuilder.build()).enqueue(MCallback(onOk, onError))
    }

    /**
     * Method to make HTTP GET request
     * @param url String site url
     * @return HttpResponse
     */
    fun syncGet(url: String):HttpResponse{
        val resp = HttpResponse(url, -1, HashMap(), "")
        var response:Response? = null

        try {
            client = buildClient()

            val requestBuilder = Request.Builder().url(url)
            headers.forEach { (t, u) ->
                requestBuilder.addHeader(t, u)
            }

            response = client.newCall(requestBuilder.build()).execute()

            resp.code = response.code
            response.headers.forEach {
                resp.header[it.first] = it.second
            }
            resp.body = response.body?.string()?:""

        }catch (e:Exception){
            if(printExceptions)
                e.printStackTrace()
        }finally {
            try {
                response?.close()
            }catch (e: Exception){}
        }

        return resp
    }

    /**
     * Method to make HTTP POST request
     * @param url site
     * @param data Hashmap<String, String> post data
     * @param onOk Unit, calls when site returns any response
     * @param onError Unit, calls when was any exception
     */
    fun asyncPost(url: String, data: HashMap<String, String>, onOk:(HttpResponse)->Unit, onError:(e: Exception)->Unit = {}){
        client = buildClient()
        val formBody = FormBody.Builder()

        data.forEach{
            formBody.add(it.key, it.value)
        }

        val requestBuilder = Request.Builder().post(formBody.build()).url(url)
        headers.forEach { (t, u) ->
            requestBuilder.addHeader(t, u)
        }

        client.newCall(requestBuilder.build()).enqueue(MCallback(onOk, onError))
    }

    /**
     * Method to make sync HTTP POST request
     * @param url String
     * @param data Hashmap<String, String> post data
     * @return HttpResponse
     */
    fun syncPost(url: String, data: HashMap<String, String>): HttpResponse{
        val resp = HttpResponse(url, -1, HashMap(), "")
        var response:Response? = null

        try {
            client = buildClient()
            val formBody = FormBody.Builder()

            data.forEach{
                formBody.add(it.key, it.value)
            }

            val requestBuilder = Request.Builder().post(formBody.build()).url(url)
            headers.forEach { (t, u) ->
                requestBuilder.addHeader(t, u)
            }

            response = client.newCall(requestBuilder.build()).execute()
            resp.code = response.code
            response.headers.forEach {
                resp.header[it.first] = it.second
            }
            resp.body = response.body?.string()?:""
        }catch (e:Exception){
            if(printExceptions)
                e.printStackTrace()
        }finally {
            try {
                response?.close()
            }catch (e: Exception){}
        }

        return resp
    }

    /**
     * Method to make sync upload file
     * @param url String
     * @param data Hashmap<String, String> post data
     * @param fileKey String key of file for multipart Body
     * @param files List<File> list files
     */
    fun uploadFileSync(url: String, data: HashMap<String, String>, fileKey:String = "files[]", files:List<File>): HttpResponse{
        val r = MultipartBody.Builder().setType(MultipartBody.FORM)
        val resp = HttpResponse(url, -1, HashMap(), "")
        var okResp:Response? = null

        try {
            files.forEach {
                r.addFormDataPart(fileKey, it.name, it.asRequestBody("text/php".toMediaTypeOrNull()))
            }

            data.forEach {
                r.addFormDataPart(it.key, it.value)
            }

            val request: Request = Request.Builder()
                .url(url)
                .post(r.build())
                .build()

            okResp = client.newCall(request).execute()

            resp.code = okResp.code
            okResp.headers.forEach {
                resp.header[it.first] = it.second
            }
            resp.body = okResp.body?.string()?:""

        }catch (e:Exception){
            if(printExceptions)
                e.printStackTrace()
        }finally {
            try {
                okResp?.close()
            }catch (e: Exception){}
        }

        return resp
    }

    /**
     * Method to make async upload file
     * @param url site
     * @param data Hashmap<String, String> post data
     * @param fileKey String key of file for multipart Body
     * @param mediaType String, type of uploading file ex. "text/php"
     * @param files List<File> list files
     */
    fun uploadFileAsync(url: String, data: HashMap<String, String>, fileKey:String = "files[]", mediaType: String = "text/php", files:List<File>, onOk:(HttpResponse)->Unit, onError:(e: Exception)->Unit = {}){
        try {
            val r = MultipartBody.Builder().setType(MultipartBody.FORM)
            files.forEach {
                r.addFormDataPart(fileKey, it.name, it.asRequestBody(mediaType.toMediaTypeOrNull()))
            }

            data.forEach {
                r.addFormDataPart(it.key, it.value)
            }

            val request: Request = Request.Builder()
                .url(url)
                .post(r.build())
                .build()

            client.newCall(request).enqueue(MCallback(onOk, onError))

        }catch (e:Exception){
            if(printExceptions)
                e.printStackTrace()
        }
    }




}
package com.scanner.core.http

/**
 * Class for Http response
 * @see HttpResponse
 * @author minelife
 */
data class HttpResponse(
    var url:String,
    var code:Int,
    var header:HashMap<String, String>,
    var body:String
)
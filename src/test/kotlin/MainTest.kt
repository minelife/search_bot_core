import javafx.application.Application

object MainTest {

    @JvmStatic
    fun main(vararg args: String){
        try {
            Application.launch(TestApplication::class.java)
        }catch (e: Exception){e.printStackTrace()}
    }

}
import com.scanner.core.ui.UIApplication
import javafx.stage.Stage

class TestApplication: UIApplication(){

    override fun start(primaryStage: Stage?) {
        TestPage().start()
    }

}
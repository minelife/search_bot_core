import com.scanner.core.ui.controllers.UISettingsPageController
import javafx.fxml.FXML
import javafx.scene.layout.VBox

class SettingsController :  UISettingsPageController(){

    @FXML
    lateinit var settingsRoot :VBox

    override fun getRootNodeForParams(): VBox = settingsRoot
}
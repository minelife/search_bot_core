<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#included-libraries">Included libraries</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
        <li><a href="#usage">Usage</a></li>
        <li><a href="#classes">Classes</a></li>
        <li><a href="#how-to-create-module">How to create module</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

This project is the core for searching bot. This can be used for writing site vulnerability scanner and for mass exploit (It is just theory 😈) .
Current library contains small logic for writing all stuff for scanning and saving result to db or file.

### Included libraries

* [JavaFX](https://openjfx.io)
* [Kotlin](https://kotlinlang.org)
* [OkHttp](https://square.github.io/okhttp/)
* [Gson](https://github.com/google/gson)
* [Ebean](https://ebean.io)
* [Event System](https://github.com/ForYaSee/EventSystem)
* [Console Progress Bar](https://github.com/ctongfei/progressbar)



<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

Just have installed any code editor.
And have base knowledges about JavaFX, or not :)

### Installation

You can use only jar file as library. [DOWNLOAD HERE](https://gitlab.com/minelife/search_bot_core/-/jobs/artifacts/master/download?job=jar)

To clone the repo use
   ```sh
   git clone https://gitlab.com/minelife/search_bot_core.git
   ```

<!-- USAGE EXAMPLES -->

### Usage

`How it works`<br /> <br />
 You are downloading the `main.jar` file from [this project](https://gitlab.com/minelife/search_bot_main) (Project not inited yet). This file is the main launcher, it looks for all the files in the `modules` folder (there are all exploits, scanners, etc.) and shows the whole graphical interface, read more about this on the [project page](https://gitlab.com/minelife/search_bot_main).
 All modules must use this core and be compiled into a jar file. All modules must contain `moduleConfig.json` file in root folder of `jar` package. This file contain all info about module such as `name`, `startPage`, `menuPath`, `params`. `startPage` this field provide launcher info about which class is the main for this module. `menuPath` - provides info how to add this module to the menu, for example if `menuPath` equal to "CMS/exploits" then will be created a button with name "CMS" in main group. When press this button than will be opened second menu. To second menu will be added "exploits" button and if press on this button will be third menu with button named as this module. If there are 2 modules with same `menuPath` value, they will all be added to the same group. This means that after clicking on the "exploits" button, 2 buttons with the names of the modules will be created.

### Classes

    com.scanner.core.database.`MDatabase` - Abstract Class for creating connection with database.
    com.scanner.core.database.models.`BaseDbModel` - Base class for db model class
    com.scanner.core.database.models.`*` - there are some other database models

    com.scanner.core.http.`HttpHelper` - This helper class for send post, get, upload files...
    com.scanner.core.http.`HttpResponse` - This model for http response
    
    com.scanner.core.ui.controllers.`UISettingsPageController` - This controller for settings page, it's automaticaly creats all fields from global params provider
    
    com.scanner.core.ui.dialogs.`UIDialog` - Class helper for create any dialogs
    
    com.scanner.core.ui.list.`TasksListFactory` - Factory for creating list with progress bar
    com.scanner.core.ui.list.`UICustomListFactory` - Class helper for creating custom item for list
    
    com.scanner.core.ui.pages.`UIPage` - Base Abstract class for creating any page which <b>don't</b> receive arguments
    com.scanner.core.ui.pages.`UIPageWithArgs` - Base Abstract class for any page which receive any type of arguments
    com.scanner.core.ui.pages.`UIPageWithFileArg` - Base Abstract class for any page which receive `File` as argument 
    com.scanner.core.ui.pages.`UIPageWithList` - Base Abstract class for any page which receive `List<T>` as argument 
    com.scanner.core.ui.pages.`UIPageWithStringArg` - Base Abstract class for any page which receive `String` as argument 

    com.scanner.core.ui.view.`NumberTextField` - This is TextField which receive only numbers
    com.scanner.core.ui.view.`UIBaseNode` - This is abstract class for creating own `Parent` node 
    com.scanner.core.ui.view.`UIProgressNode` - This class which returns `Parent` for showing page with progress, just set it to view 
    com.scanner.core.ui.view.`UIScanMenuNode` - This class which returns `Parent` to build menu for module

    com.scanner.core.utils.`GlobalParamsProvider` - This class provides functional for load params from settings page
    com.scanner.core.utils.`Helper` - Add some functional to primitive classes
    com.scanner.core.utils.`JarLoader` - Class which helps to load classes and call funnctions from jar files
    com.scanner.core.utils.`Log` - Class for out colored logs to console
    com.scanner.core.utils.`MExecutors` - Class that provides base functions for working with java executors

    com.scanner.core.ui.`UIApplication` - Abstrat class for create application, don't use this in moddule
    com.scanner.core.ui.`UIController` - Abstract class for creating any controllers for page

### How to create module

1. Create empty java/kotlin project.
2. Include core.jar as library.
3. Create `class` which extends from `UIPage` and set it as main in `moduleConfig.json` file.
4. Create class which will have, this class should show menu of this module or your logic. If you want to use default menu just create instance of `UIScanMenuNode`.
5. Write all logic.
6. Build `jar`.

  `UIPage` have 2 function to load page nodes (templates): `loadWithNode`, `loadXml`.

  `loadWithNode` - initialise all necessary variables and sets the passed node value as view.
  `loadXml` - initialise all necessary variables and load fxml file than set fxml node as view.

  Example of main Page:

    class MainPage: UIPage() {

      override fun start() {
        val node = UIScanMenuNode()
              .setTitle("WP Like button menu")
              .addBaseScanMenu(object : UIMainExploitMenuViewCallback {
              override fun onScanOneSite(url: String) {
                  //Your logic, calls when selected to scan one site
              }

              override fun onScanFileListSite(file: File) {
                  //Your logic , calls when selected to scan list sites
              }

              override fun onScanSitesFromDB() {
                  //Your logic, calls when selected to scan sites from db
              }

          }).addExitButton().getNode()
          loadWithNode(node, Stage())
      }

    }

<!-- LICENSE -->
## License
Free License
You can use this in any project.

<!-- CONTACT -->
## Contact

Minelife - telegram: [@minelifes](https://t.me/minelifes)

Project Link: [https://gitlab.com/minelife/search_bot_core](https://gitlab.com/minelife/search_bot_core)
Main Runner Link: [https://gitlab.com/minelife/search_bot_main](https://gitlab.com/minelife/search_bot_main)
